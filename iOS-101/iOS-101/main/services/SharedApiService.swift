//
//  SharedApiService.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 17/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import Foundation

class SharedApiService: NSObject {
    static let sharedInstance: SharedApiService = {SharedApiService()}()
    
    private(set) var articleService: ArticleService
    
    private override init() {
        self.articleService = ArticleService()
    }
}
