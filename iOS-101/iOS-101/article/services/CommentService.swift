//
//  CommentService.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 18/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire
typealias CommentsCompletion = (_ comments: [CommentModel]?,_ error: String?) -> Void

class CommentService: TypicodeApiService {
    func comments(postId: Int, completion: @escaping CommentsCompletion){
        let url = host + "/comments"
        
        var params: [String: AnyObject] = [:]
        params["postId"] = postId as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    
                    completion(nil, error!.localizedDescription)
                    return
                }else if let comments = responseData!.arrayObject{
                    
                    let commentsModel = Mapper<CommentModel>().mapArray(JSONObject: comments)
                    completion(commentsModel, nil)
                }
                completion(nil,"Ошибка загрузки комментариев")
        }
        
        
    }
}
