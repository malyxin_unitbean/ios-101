//
//  ArticleService.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 17/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ArticlePostCompletion = (_ articles: [ArticleModel]?, _ error: String?) -> Void

class ArticleService: TypicodeApiService {
    
    func posts(limit: Int, skip: Int, completion: @escaping ArticlePostCompletion){
        
        let url = host + "/posts"
        
        var params:[String: AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                    
                }else if let articles = responseData!.arrayObject {
                    let articlerModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
                    
                    completion(articlerModels, nil)
                    return
                }
                
                completion(nil,"Ошибка загрузки каталога")
        }
    }
}
