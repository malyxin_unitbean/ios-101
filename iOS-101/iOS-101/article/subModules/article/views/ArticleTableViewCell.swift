//
//  ArticleTableViewCell.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 16/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    static let nibName = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    func customize(article: ArticleModel){
        
        titleLable.text = article.title
        bodyLabel.text = article.body
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
