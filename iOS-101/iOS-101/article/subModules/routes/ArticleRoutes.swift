//
//  ArticleRoutes.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 16/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import Foundation
import UIKit

class ArticleRoutes {
    
    static func showArticle(fromVc: UIViewController){
        
        let toVC = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticleTableViewController.nibName)
        fromVc.navigationController?.pushViewController(toVC, animated: true)
    }
    
}
