//
//  ArticleTitleTableViewCell.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 16/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import UIKit

class ArticleTitleTableViewCell: UITableViewCell {
    static let nibName = "ArticleTitleTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
