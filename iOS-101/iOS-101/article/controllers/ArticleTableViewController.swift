//
//  ArticleTableViewController.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 16/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import UIKit

class ArticleTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    static let nibName = "ArticleTableViewController"
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        customizeTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func customizeTable(){
        tableView.register(
            UINib(nibName: ArticleTitleTableViewCell.nibName, bundle: nil),
            forCellReuseIdentifier: ArticleTitleTableViewCell.nibName)
        
        tableView.register(
            UINib(nibName: ArticleContentTableViewCell.nibName, bundle: nil),
            forCellReuseIdentifier: ArticleContentTableViewCell.nibName)
        
        tableView.register(
            UINib(nibName: CommentTableViewCell.nibName, bundle: nil),
            forCellReuseIdentifier: CommentTableViewCell.nibName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 117
        }else if indexPath.section == 1 {
            return 798
        }else {
            return 76
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 1
        }else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTitleTableViewCell.nibName)
            as! ArticleTitleTableViewCell
        return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleContentTableViewCell.nibName)
                as! ArticleContentTableViewCell
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.nibName)
                as! CommentTableViewCell
            return cell
            
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

