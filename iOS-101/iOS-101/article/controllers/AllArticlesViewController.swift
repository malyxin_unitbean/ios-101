//
//  AllArticlesViewController.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 16/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import UIKit

class AllArticlesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,
AllArticlesDataProviderDelegate{
    
    
    
    var dataProvider: AllArticlesDataProvider = AllArticlesDataProvider()
    //MARK: Params
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.delegate = self
        customizeTableView()
        dataProvider.loadArticles()
        // Do any additional setup after loading the view.
    }
    
    //MARK: TableView
    func customizeTableView (){
        tableView.register(
            UINib(nibName: ArticleTableViewCell.nibName, bundle: nil),
            forCellReuseIdentifier: ArticleTableViewCell.nibName)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.nibName) as! ArticleTableViewCell
        cell.customize(article: dataProvider.articles[indexPath.row])
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 344
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // performSegue(withIdentifier: "toArticle", sender: nil)
        ArticleRoutes.showArticle(fromVc: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - AllArticlesDataProviderDelegate
    
    func articlesDataLoaded() {
        tableView.reloadData()
    }
    
    func articlesDataHasError(error: String) {
        //TODO: реализовать обработку ошибки
    }
    
}
