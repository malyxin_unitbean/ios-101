//
//  AllArticlesDataProvider.swift
//  iOS-101
//
//  Created by Antonij Malyxin on 18/04/2018.
//  Copyright © 2018 Antonij Malyxin. All rights reserved.
//

import Foundation

protocol AllArticlesDataProviderDelegate: class {
    func articlesDataLoaded()
    func articlesDataHasError(error: String)
}

class AllArticlesDataProvider: NSObject {
    
    var articles: [ArticleModel] = []
    
    weak var delegate: AllArticlesDataProviderDelegate?
    
    func refresh() {
        articles = []
        loadArticles()
        
    }
    
    func loadArticles(){
        
        SharedApiService.sharedInstance.articleService.posts(
            limit: 100,
            skip: 0) { (articlesResponse, errors) in
                if let error = errors {
                    self.delegate?.articlesDataHasError(error: error)
                    return
                }else if let articles:[ArticleModel] = articlesResponse{
                    self.articles.append(contentsOf: articles)
                    self.delegate?.articlesDataLoaded()
                    return
                    
                }
                self.delegate?.articlesDataLoaded()
        }
        
        
    }
}
